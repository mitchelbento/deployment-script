#!/bin/sh

echo "Do you want to commit your current staged changes, including any updates to package.json?"
echo
select_option "" "Yes" "No"
choice=$?

if [[ $choice -eq 0 ]]; then
    git fetch > /dev/null 2>&1

    branch=$(git rev-parse --abbrev-ref HEAD)

    if [[ $version != $prev_version ]]; then
        git add ./package.json > /dev/null 2>&1
        version_mismatch=true
    else
        version_mismatch=false
    fi

    if [[ ! -z $(git diff --name-only --cached) ]]; then
        echo "Enter in message for commit:"
        echo

        read message
        echo

        if [[ $version_mismatch == true ]]; then
            desc="$prev_version to $version"
        else
            desc="$prev_version"
        fi

        if [[ ${#message} -eq 0 ]]; then
            git commit -m "Upgrading version $desc. Automatically committed by @bentosmb/deploy." > /dev/null 2>&1
        else
            git commit -m "$message" -m "Upgrading version $desc. Automatically committed by @bentosmb/deploy." > /dev/null 2>&1
        fi

        git push origin $branch > /dev/null 2>&1
    fi

    # Check if user is deploying a new public version
    if ([[ $branch == 'develop' ]] || [[ $branch == 'master' ]]) && [[ $choice_text_lowercase == *"prod"* ]]; then
        echo "Do you want to create a new stable branch?"
        echo
        select_option "" "Yes" "No"
        choice=$?

        if [[ $choice -eq 0 ]]; then
            echo "Pushing stable branch for version $version"
            echo
            new_branch="stable-$version"
            git checkout -b $new_branch $branch > /dev/null 2>&1 || \
            ( \
                git checkout $new_branch > /dev/null 2>&1
                git merge $branch > /dev/null 2>&1 || \
                ( \
                    echo "Failed to update stable branch..."
                    git checkout $branch > /dev/null 2>&1
                    exit 0
                )
            )
            
            echo "Returning back to '$branch' branch..."
            echo
            git push origin $new_branch > /dev/null 2>&1
            git checkout $branch > /dev/null 2>&1
        fi
    fi

    git fetch > /dev/null 2>&1
fi