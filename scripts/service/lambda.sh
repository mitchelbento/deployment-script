#!/bin/sh

###########################################
### -- Lambda Service Implementation -- ###
###########################################

# Archive project assets into zip
echo "Archiving project assets, please wait..."
echo
zip -r -q archive.zip *

# Get lambda environment variables
( response=$( \
    aws --profile=${profile} \
    --region=${region} \
    lambda get-function-configuration \
    --function-name ${endpoint[$choice]} \
)

# Get variables from response
response=$( \
    sed 's/"Variables": {/&\n/;s/.*\n//;s/}/\n&/;s/\n.*//' \
    <<< $response \
)

# Check if VERSION exists in the reponse
response_version=$( \
    sed 's/"VERSION":/&\n/;s/.*\n//;s/,/\n&/;s/\n.*//;s/"//g;s/ //g' \
    <<< $response \
)

# Check if the current version on lambda exists, else add version
if [[ response_version =~ ^([0-9]+\.){2}(\*|[0-9]+)$ ]]
then
    response=$( \
        sed "s/$response_version/$version/g" \
    )
else
    response="$response, \"VERSION\": \"$version\""
fi \
) || echo "Failed to get environment variables from Lambda, skipping..."

echo "Uploading archive to lambda, this may take a moment..."
echo
(aws --profile=${profile} \
    --region=${region} \
    lambda update-function-code \
    --function-name ${endpoint[$choice]} \
    --zip-file fileb://archive.zip \
    > /dev/null \
    && (echo "Successfully deployed package at version $version...") \
    || (echo "Failed to deploy package..." && exit 1) \
)
echo

# Update lambda environment variable
if [[ ! -z $request ]]; then
aws --profile=${profile} \
    --region=${region} \
    lambda update-function-configuration \
    --function-name ${endpoint[$choice]} \
    --environment "{\"Variables\":{$response}}" \
    > /dev/null
else
    echo "Skipped updating Lambda environment due to error..."
fi