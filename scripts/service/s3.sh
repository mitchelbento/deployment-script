#!/bin/sh

#######################################
### -- S3 Service Implementation -- ###
#######################################

# Backup current version of the project
aws --profile=${profile} \
    --region=${region} \
    s3 cp \
    "${endpoint[$choice]}/" \
    "${endpoint[$choice]}-$prev_version/" \
    --recursive

# Build project before uploading
react-scripts build

# Upload new version of the project
aws --profile=${profile} \
    --region=${region} \
    s3 sync \
    build/ \
    "${endpoint[$choice]}/"