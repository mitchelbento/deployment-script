#!/bin/sh
##################################################
### -- EDITING PAST THIS POINT MAY BE FATAL -- ###
##################################################

DIR="./node_modules/@bentosmb/deploy"

# Check if config file exsists, else generate default
FILE=deploy.conf
if test -f "$FILE"; then
    . $FILE
else
    echo "No configuration file located, generating default file..."
    cp $DIR/config-example.conf "$FILE"
    exit 0;
fi

# Check request for flags
. $DIR/scripts/check_flags.sh

# Include Select Options
. $DIR/scripts/select_option.sh

echo "Select one option using up/down keys and enter to confirm:"
echo

select_option "Deploy" "${key[@]}" "Cancel"
choice=$?

# Check if user chose 'Cancel', then exit
if [ $choice -eq ${#key[@]} ]; then 
    echo "Exiting deployment process, goodbye..."
    exit 0
fi

choice_text=${key[$choice]}
choice_text_lowercase=$(awk '{print tolower($0)}' <<< $choice_text)

# Confirm selection
if [[ $choice_text_lowercase == *"prod"* ]]; then
    echo "Are you sure you want to deploy to $choice_text:"
    echo
    select_option "" "Yes" "No"
    choice=$?

    case $choice in
    0)
        echo "Deploying to $choice_text..."
        ;;
    1)
        echo "Exiting deployment, Goodbye..."
        exit 0
        ;;
    esac
fi

# Get current version of project from package.json
prev_version="`grep '"version"' package.json | sed -r 's/^[^:]*:(.*)$/\1/' | sed 's/"//g' | sed 's/,//g' | sed 's/ //g'`"
echo "Version: $prev_version"
echo


if [[ $quick != true ]]; then
    # Increment project version
    echo "Should the version number be incremented or set to a specific value?"
    echo "( Yes(y) / No(n) / [0-9].[0-9].[0-9] Custom )"
    echo

    while true; do
        read input

        [[ $input == "n" ]] \
        && version="$prev_version" \
        && break

        [[ $input == "y" ]] \
        && version="`echo $prev_version | awk -F. -v OFS=. 'NF==1{print ++$NF}; NF>1{if(length($NF+1)>length($NF))$(NF-1)++; $NF=sprintf("%0*d", length($NF), ($NF+1)%(10^length($NF))); print}'`" \
        && break

        [[ $input =~ ^([0-9]+\.){2}(\*|[0-9]+)$ ]] \
        && version=$input \
        && break

        echo "Invalid response, try again..."
        echo
    done

    echo
    echo "Modifying from version $prev_version to $version..."
    echo

    # Update package.json version
    echo "Updating local package.json with new version..."
    echo
    sed -i '' "s/\"version\": *\"$prev_version\"/\"version\": \"$version\"/g" package.json
fi

# Run service
. $DIR/scripts/service/$service.sh

# If quick mode, then skip rest of process
[[ $quick == true ]] && exit 0

. $DIR/scripts/service/git.sh

exit 0
