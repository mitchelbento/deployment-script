# Deployment Script #

## Description
Deploy project to lambda or s3 along with handling version control with your git repository.

## How to Use
- Add a new script to your `package.json`
```
...
"scripts": {
    ...
    "deploy": "deploy",
    "update": "deploy --quick",
    ...
}
...
```
- In the terminal run `npm run deploy` and if you have configured it correctly, follow the prompts and enjoy
- `npm run update` will only run deployment scripts and ignore git and version updates

## Configuration
- Install the package to project `npm install @bentosmb/deploy --save-dev`
- Create a new file in the root of your project named `deploy.conf`
- Paste the example configuration script and update to your needs
```
#!/bin/sh

#############################################
### -- CONFIGURE THE DEPLOYMENT SCRIPT -- ###
#############################################

# AWS Configuration
service=# lamdbda | s3
profile=
region=

key=(
  Development
  Staging
  Production
)

endpoint=(
  s3://com.example.dev/dir
  s3://com.example.stage/dir
  s3://com.example.prod/dir
)
```

## Warning
This script has been created for a very specific internal operation and may not work with your configuration, will not work for most projects.